#include "ServoOperator.h"

ServoOperator::ServoOperator(uint8_t servoPin) {
    updateFlow(context.getWaterFlow());
    this->servoPin = servoPin;
    tankOpen = false;
    servo.attach(servoPin);
}

void ServoOperator::off() {
    if(tankOpen) {
        Serial.println("Tank closed");
        servo.write(0);
        //servo.detach();
        updateFlow(context.getWaterFlow());
        tankOpen = false;
    }
}

void ServoOperator::on() {
    if(!tankOpen) {
        servo.write(open_angle);
        updateFlow(context.getWaterFlow());
        tankOpen = true;
    }
}
