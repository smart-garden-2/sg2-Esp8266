#include "DataGatherer.h"

DataGatherer::DataGatherer(SoilMoisture* soilMoisture, uint32_t soilDrynessSimulated) {
//    this->soilMoisture = soilMoisture;
    this->soilDrynessSimulated = soilDrynessSimulated;
    lastSoilDrynessSwap = 0;
    updateFreq(context.getDataGatheringFrequency());
    randomSeed(analogRead(0));
}

void DataGatherer::execute() {
    updateFreq(context.getDataGatheringFrequency());
    uint32_t t = millis();
    uint32_t lastTime = context.getLastDataGatherTime();

    if(t - lastTime >= dt) {
        auto airHumidity = (float) random(70, 90); //bme->getHumidity();
        uint32_t soilHumidity = soilDrynessSimulated; //soilMoisture->getSoilHumidity();
        auto temperature = (float) random(26, 30); //bme->getTemperature();
        uint32_t pressure =  random(95000, 110000); //bme->getPressure();

        if (isnan(airHumidity) || isnan(soilHumidity)
            || isnan(temperature) || isnan(pressure)) {
            IOSerialClass::sendMsg(
                    "[DataGatherer]: Invalid sensor readings");
        } else {
            publishData(airHumidity,
                        soilHumidity,
                        temperature,
                        pressure);
        }

        context.updateSoilHumidity(soilHumidity);
        context.updateLastDataGatherTime();
    }

    uint32_t tNow = millis();
    if(tNow - lastSoilDrynessSwap >= 3203) {
        if(soilDrynessSimulated == 735) {
            soilDrynessSimulated = 235;
        } else {
            soilDrynessSimulated = 735;
        }
        Serial.println(soilDrynessSimulated);
        lastSoilDrynessSwap = tNow;
    }
}

void DataGatherer::publishData(
        float airHumidity,
        uint32_t soilHumidity,
        float temperature,
        uint32_t pressure) {

    time(&now);
    StaticJsonDocument<384> telemetryData;

    telemetryData["timestamp"] = now;

    JsonArray metrics = telemetryData.createNestedArray("metrics");

    JsonObject metrics_0 = metrics.createNestedObject();
    metrics_0["name"] = "airHumidity";
    metrics_0["value"] = airHumidity;

    JsonObject metrics_1 = metrics.createNestedObject();
    metrics_1["name"] = "pressure";
    metrics_1["value"] = pressure;

    JsonObject metrics_2 = metrics.createNestedObject();
    metrics_2["name"] = "temperature";
    metrics_2["value"] = temperature;

    JsonObject metrics_3 = metrics.createNestedObject();
    metrics_3["name"] = "soilHumidity";
    metrics_3["value"] = soilHumidity;

    IOMQTTClass::publish(telemetryData, "system/telemetry");
}
