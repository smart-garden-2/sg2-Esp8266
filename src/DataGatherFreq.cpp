#include <cstring>
#include "DataGatherFreq.h"

DataGatherFreq getGatherFreq(const char* metric) {
    if(strcmp(metric, "each_second") == 0) {
        return EACH_SECOND;
    } else if(strcmp(metric, "each_minute") == 0) {
        return EACH_MINUTE;
    } else if(strcmp(metric, "each_quarter") == 0) {
        return EACH_QUARTER;
    } else if(strcmp(metric, "each_hour") == 0) {
        return EACH_HOUR;
    } else if(strcmp(metric, "each_day") == 0) {
        return EACH_DAY;
    }
    return EACH_MINUTE;
}
