#include "IOMQTT.h"

String mqttContent;
IOMQTTClass IOMQTT;
WiFiClient wiFiClient;
PubSubClient mqttClient(wiFiClient);

void IOMQTTClass::init() {
    mqttContent.reserve(256);
    mqttContent = "";
    currentTopic = nullptr;
    currentPayload = nullptr;
    msgAvailable = false;
}

void callback(const char* topic, const byte* payload, unsigned int length) {
#ifdef DEBUG
    Serial.print("[IOMQTT]: topic: ");
    Serial.println(topic);
    Serial.print("[IOMQTT]: length: ");
    Serial.println(length);
    for (int i = 0; i < length; i++) {
        Serial.print((char) payload[i]);
    }
    Serial.println();
#endif
    for (uint i = 0; i < length; i++) {
        char ch = (char) payload[i];
        mqttContent += ch;
    }
    IOMQTT.currentTopic = new Msg(topic);
    IOMQTT.currentPayload = new Msg(mqttContent);
    IOMQTT.msgAvailable = true;
}

void IOMQTTClass::connect() {
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);

    // Wi-Fi connection
    WiFi.mode(WIFI_STA);
    WiFi.begin(SSSID, SPASS);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.print("Wi-Fi online: ");
    Serial.println(WiFi.localIP());

    // MQTT connection
    mqttClient.setServer(broker, port);
    mqttClient.setCallback(callback);
    mqttClient.connect(clientId.c_str());
    mqttClient.subscribe("system/soilHumidityThreshold");
    mqttClient.subscribe("system/dataGatheringFreq");
    mqttClient.subscribe("system/waterFlow");
    mqttClient.subscribe("system/sleepCmd");
    mqttClient.subscribe("system/waterCmd");
    Serial.println("Subscription through MQTT to all topics executed");
}

void IOMQTTClass::publish(const JsonDocument &jsonDocument, const char* topic) {
    char buffer[256];
    size_t n = serializeJson(jsonDocument, buffer);
    mqttClient.publish(topic, buffer, n);
}

void IOMQTTClass::publish(const char* msg, const char* topic) {
    mqttClient.publish(topic, msg);
}

void IOMQTTClass::publishMetricAsJson(
        time_t now,
        const char* metricName,
        uint8_t metricValue,
        const char* topic) {

    StaticJsonDocument<96> jsonDocument;
    jsonDocument["timestamp"] = now;
    jsonDocument["name"] = metricName;
    jsonDocument["value"] = metricValue;

    char buffer[256];
    size_t n = serializeJson(jsonDocument, buffer);
    mqttClient.publish(topic, buffer, n);
    //Serial.println(metricName);
    //Serial.println(metricValue);
}

void IOMQTTClass::subscribe(const char* topic) {
    mqttClient.subscribe(topic);
}

void IOMQTTClass::poll() {
    mqttClient.loop();
}

bool IOMQTTClass::connected() {
    return mqttClient.connected();
}

bool IOMQTTClass::isMsgAvailable() const {
    return msgAvailable;
}

Msg *IOMQTTClass::receiveMsg() {
    if (msgAvailable) {
        Msg *msg = currentPayload;
        msgAvailable = false;
        currentPayload = nullptr;
        mqttContent = "";
        return msg;
    } else {
        return nullptr;
    }
}

Msg *IOMQTTClass::receiveMsgTopic() {
    return currentTopic;
};

bool IOMQTTClass::rcvMsg(StateData& data, String& input) {
    Serial.println(input);
    strlcpy(data.topic, currentTopic->getContent().c_str(), 32);
    strlcpy(data.value, input.c_str(), 16);
    return false;
}

bool IOMQTTClass::rcvMsg(SoilHumidityData& data, String& input) {
    Serial.println(input);
    int value = atoi(input.c_str());
    strlcpy(data.topic, currentTopic->getContent().c_str(), 32);
    data.min = value;
    data.max = value;
    return false;
}

bool IOMQTTClass::rcvJson(StateData& data) {
    Msg* msg = receiveMsg();
    String msgContent = msg->getContent();
    bool status = rcvMsg(data, msgContent);
    delete msg;
    return status;
}

bool IOMQTTClass::rcvJson(SoilHumidityData& data) {
    Msg* msg = receiveMsg();
    String msgContent = msg->getContent();
    bool status = rcvMsg(data, msgContent);
    delete msg;
    return status;
}
