#include "SoilMoisture.h"

SoilMoisture::SoilMoisture(int analogPin) {
    this->analogPin = analogPin;
}

uint32_t SoilMoisture::getSoilHumidity() const {
    return analogRead(analogPin);
}
