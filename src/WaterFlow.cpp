#include <cstring>
#include "WaterFlow.h"

WaterFlow getFlow(const char* metric) {
    if(strcmp(metric, "ONE_QUARTER") == 0) {
        return ONE_QUARTER;
    } else if(strcmp(metric, "HALF") == 0) {
        return HALF;
    } else if(strcmp(metric, "THREE_QUARTER") == 0) {
        return THREE_QUARTER;
    } else if(strcmp(metric, "FULL_SPEED") == 0) {
        return FULL;
    }
    return HALF;
}
