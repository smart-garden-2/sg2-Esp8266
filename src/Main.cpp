#include "Arduino.h"
#include "Sonar.h"
#include "PinConfig.h"
#include "ManagerTask.h"
#include "TankMonitor.h"
#include "MonitoringTask.h"
#include "WateringTask.h"
#include "DataGatherer.h"
#include "ServoOperator.h"
#include "IOSerial.h"

void connectNtp() {
    int timezone = 3;
    int dst = 0;
    WiFi.mode(WIFI_STA);
    WiFi.begin(SSSID, SPASS);
    Serial.println("Connecting to Wi-Fi");
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(1000);
    }
    configTime(timezone * 3600, dst * 0, "pool.ntp.org", "time.nist.gov");
    Serial.println("Waiting for time");
    while (!time(nullptr)) {
        Serial.print(".");
        delay(1000);
    }
    Serial.println("");
}

Sonar sonar(ECHO_PIN, TRIG_PIN, 10000);
SoilMoisture soilMoisture(SOIL_MOISTURE_PIN);

uint32_t soilDrynessSimulated = 555;

void setup() {
    IOSerial.init();

    IOMQTT.connect();
    connectNtp();

    new ManagerTask(100);

    auto* tankMonitor = new TankMonitor(&sonar);
    auto* dataGatherer = new DataGatherer(&soilMoisture, soilDrynessSimulated);
    auto* servoOperator = new ServoOperator(SERVO_PIN);

    new MonitoringTask(dataGatherer, tankMonitor, 100);
    new WateringTask(servoOperator, 100);
}

void loop() {}
