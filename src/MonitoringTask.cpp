#include "MonitoringTask.h"

MonitoringTask::MonitoringTask(
        DataGatherer *dataGatherer,
        TankMonitor *tankMonitor,
        uint32_t period_ms) {

    this->dataGatherer = dataGatherer;
    this->tankMonitor = tankMonitor;
    state = IDLE;
    timer.attach_ms(period_ms,
                    +[](MonitoringTask* monTask) { monTask->tick(); }, this);
}

void MonitoringTask::setActive(bool active) {
    Task::setActive(active);
}

void MonitoringTask::tick() {
    switch (state) {
        case IDLE: {
            if(context.shouldMonitor()) {
                state = WORKING;
            }
            break;
        }
        case WORKING: {
            IOSerialClass::sendMsg("[MonitoringTask]: Working");
            dataGatherer->execute();
            tankMonitor->execute();

            if(!context.shouldMonitor()) {
                state = IDLE;
                IOSerialClass::sendMsg("[MonitoringTask]: Idle");
            }
            break;
        }
    }
}
