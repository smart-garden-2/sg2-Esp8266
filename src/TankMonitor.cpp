#include "TankMonitor.h"

TankMonitor::TankMonitor(Sonar* sonar) {
    this->sonar = sonar;
//    this->bme = bme;
    updateFreq(context.getDataGatheringFrequency());
}

void TankMonitor::execute() {
    updateFreq(context.getDataGatheringFrequency());
    uint32_t t = millis();
    uint32_t lastTime = context.getLastTankLevelUpdateTime();
    if (t - lastTime >= dt) {
        float distance = sonar->getDistance();
        Serial.println(distance);
        if (isnan(distance)) {
            IOSerialClass::sendMsg(
                    "[TankMonitor] : Invalid distance reading");
        } else {
            time(&now);
            StaticJsonDocument<128> telemetryData;
            telemetryData["timestamp"] = now;

            JsonObject metrics_0 =
                    telemetryData["metrics"].createNestedObject();
            metrics_0["name"] = "tankLevel";
            metrics_0["value"] = distance;

            IOMQTTClass::publish(telemetryData, "system/tankLevel");
        }
        context.updateLastTankLevelUpdateTime();
    }
}
