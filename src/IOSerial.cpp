#include "IOSerial.h"

String content;
IOSerialClass IOSerial;

bool IOSerialClass::isMsgAvailable() const {
    return msgAvailable;
}

Msg *IOSerialClass::receiveMsg() {
    if (msgAvailable) {
        Msg *msg = currentMsg;
        msgAvailable = false;
        currentMsg = nullptr;
        content = "";
        return msg;
    } else {
        return nullptr;
    }
}

void IOSerialClass::init() {
    Serial.begin(9600);
    content.reserve(256);
    content = "";
    currentMsg = nullptr;
    msgAvailable = false;
}

void IOSerialClass::sendMsg(const String &msg) {
#ifdef DEBUG
    Serial.println(msg);
#endif
}

void serialEvent() {
    /* reading the content */
    while (Serial.available()) {
        char ch = (char) Serial.read();
        if (ch == '\n') {
            IOSerial.currentMsg = new Msg(content);
            IOSerial.msgAvailable = true;
        } else {
            content += ch;
        }
    }
}
