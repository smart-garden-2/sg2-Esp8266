#include "ManagerTask.h"

ManagerTask::ManagerTask(uint32_t period_ms) {
    state = EnumState::MONITOR;
    context.setMonitoring(true);
    timeWatering = context.getDataGatheringFrequency() * 2;
    timeUntilRetry = context.getDataGatheringFrequency();
    connTries = 0;
    maxConnTries = maxReconnectionAttempts;
    alarmTries = 0;
    maxAlarmTries = maxAlarmAttempts;
    lastAlarmAttempt = 0;
    lastWateringStopAttempt = 0;
    timer.attach_ms(period_ms,
                    +[](ManagerTask *manTask) { manTask->tick(); }, this);
}

void ManagerTask::tick() {
    checkInbox();
    IOMQTTClass::poll();

    if (context.shouldSleep()) {
        context.setSleeping(false);
        state = EnumState::IDLE;
        Serial.println("[IDLE]");
        EspClass::deepSleep(standardSleepTime);
    } else {
        if (!IOMQTTClass::connected()) {
            context.setSleeping(true);
        } else if (context.shouldWaterManually()) {
            publishWateringStateUpdate(EnumState::WATERING_MANUAL);
            //Serial.println("[WATERING - MANUAL]");
            state = EnumState::WATERING_MANUAL;
        }
    }

    switch (state) {
        case EnumState::IDLE: {
            state = EnumState::MONITOR;
            break;
        }
        case EnumState::WATERING_MANUAL:
            break;
        case EnumState::WATERING_AUTOMATIC: {
            if (context.isSoilHumidityHigh()) {
                context.setAutoWatering(false);
                publishWateringStateUpdate(EnumState::MONITOR);
                Serial.println("[MONITOR]");    
                state = EnumState::MONITOR;    
            }
            break;
        }
        case EnumState::MONITOR: {
            if (context.isSoilHumidityLow()) {
                publishStateUpdate(EnumState::ALARM);
                Serial.println("[ALARM]");
                state = EnumState::ALARM;
            }
            break;
        }
        case EnumState::ALARM: {
            if (context.isSoilHumidityLow()) {
                if (alarmTries < maxAlarmTries) {
                    uint32_t tnow = millis();
                    if (tnow - lastAlarmAttempt > timeUntilRetry) {
                        lastAlarmAttempt = tnow;
                        alarmTries++;
                    }
                } else {
                    publishWateringStateUpdate(
                            EnumState::WATERING_AUTOMATIC);
                    Serial.println("[WATERING - AUTOMATIC]");
                    state = EnumState::WATERING_AUTOMATIC;

                    alarmTries = 0;
                    context.setAutoWatering(true);
                }
            } else {
                publishStateUpdate(EnumState::MONITOR);
                Serial.println("[MONITOR]");
                state = EnumState::MONITOR;

                alarmTries = 0;
            }
            break;
        }
    }
}

uint8_t ManagerTask::getSystemState(EnumState stateValue) {
    switch (stateValue) {
        case EnumState::IDLE:
            return 0;
        case EnumState::WATERING_MANUAL:
            return 1;
        case EnumState::WATERING_AUTOMATIC:
            return 2;    
        case EnumState::MONITOR:
            return 3;
        case EnumState::ALARM:
            return 4;
    }
    return -1;
}

void ManagerTask::checkInbox() {
    if (IOMQTT.isMsgAvailable()) {
        if (strcmp(
                IOMQTT.receiveMsgTopic()->getContent().c_str(),
                "system/soilHumidityThreshold") == 0) {
            SoilHumidityData data{};
            IOMQTT.rcvJson(data);
            context.updateSoilHumidityThreshold(data.min, data.max);
        } else {
            StateData data{};
            IOMQTT.rcvJson(data);
            if (strcmp(
                    data.topic, "system/waterFlow") == 0) {
                context.updateWaterFlow(getFlow(data.value));
            } else if (strcmp(
                    data.topic, "system/dataGatheringFreq") == 0) {
                context.updateDataGatheringFrequency(
                        getGatherFreq(data.value));
                state = EnumState::MONITOR;
            } else if (strcmp(
                    data.topic, "system/sleepCmd") == 0) {
                if (strcmp(data.value, "1") == 0) {
                    context.setSleeping(true);
                }
            } else if (strcmp(
                    data.topic, "system/waterCmd") == 0) {
                if (strcmp(data.value, "true") == 0) {
                    context.setManualWatering(true);
                } else if (strcmp(data.value, "false") == 0) {
                    context.setManualWatering(false);
                    state = EnumState::MONITOR;
                }
            } else {
                Serial.println("Not recognized topic");
            }
        }
    }
}

void ManagerTask::publishStateUpdate(EnumState stateValue) {
    time(&now);
    IOMQTTClass::publishMetricAsJson(
            now,
            "state",
            getSystemState(stateValue),
            "system/state");
}

void ManagerTask::publishWateringStateUpdate(EnumState stateValue) {
    time(&now);
    IOMQTTClass::publishMetricAsJson(
            now,
            "state",
            getSystemState(stateValue),
            "system/state");
}