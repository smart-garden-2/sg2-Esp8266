#include "Sonar.h"

Sonar::Sonar(int echoPin, int trigPin, uint32_t timeOut) {
    this->echoPin = echoPin;
    this->trigPin = trigPin;
    this->timeOut = timeOut;
    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);
    temperature = 20;
}

float Sonar::getDistance() {

    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(5);
    digitalWrite(trigPin, LOW);

    float tUS = pulseIn(echoPin, HIGH);
    if (tUS == 0) {
        return -1;
    } else {
        float t = tUS / 1000.0 / 1000.0 / 2;
        float d = t*getSoundSpeed();
        return d;
    }
}

void Sonar::setTemperature(float temp) {
    temperature = temp;
}

float Sonar::getSoundSpeed() const {
    return 331.5 + 0.6*temperature;
}
