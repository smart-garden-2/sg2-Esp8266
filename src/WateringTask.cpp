#include "WateringTask.h"

WateringTask::WateringTask(ServoOperator *servoOperator,
                           uint32_t period_ms) {
    this->servoOperator = servoOperator;
    state = IDLE;
    timer.attach_ms(period_ms,
                    +[](WateringTask* watTask) { watTask->tick(); }, this);
}

void WateringTask::setActive(bool active) {
    Task::setActive(active);
}

void WateringTask::tick() {
    switch (state) {
        case IDLE: {
            if(context.shouldWaterManually() || context.shouldWaterAutomatically()) {
                state = WORKING;    
            }
            break;
        }
        case WORKING: {
            IOSerialClass::sendMsg("[WateringTask]: Working");
            servoOperator->on();

            if(!context.shouldWaterManually() && !context.shouldWaterAutomatically()) {
                state = IDLE;
                IOSerialClass::sendMsg("[WateringTask]: Idle");
                servoOperator->off();
            }
            break;
        }
    }
}
