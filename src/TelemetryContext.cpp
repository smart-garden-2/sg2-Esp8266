#include "TelemetryContext.h"

TelemetryContext context;

TelemetryContext::TelemetryContext() {
    soilDryness = 0;
    soilHumidityMinThreshold = 555;
    soilHumidityMaxThreshold = 400;
    dataGatheringFrequency = EACH_SECOND;
    waterFlow = HALF;
    sleepTime = false;
    shouldAutoWater = false;
    monitorTime = false;
    shouldManuallyWater = false;
    lastDataGatherTime = 0;
    lastTankLevelUpdateTime = 0;
}

void TelemetryContext::updateSoilHumidity(uint32_t newSoilDryness) {
    soilDryness = newSoilDryness;
}

void
TelemetryContext::updateSoilHumidityThreshold(
        uint32_t newSoilHumidityMinThreshold,
        uint32_t newSoilHumidityMaxThreshold) {
    soilHumidityMinThreshold = newSoilHumidityMinThreshold;
    soilHumidityMaxThreshold = newSoilHumidityMaxThreshold;
}

DataGatherFreq TelemetryContext::getDataGatheringFrequency() const {
    return dataGatheringFrequency;
}

WaterFlow TelemetryContext::getWaterFlow() const {
    return waterFlow;
}

void TelemetryContext::updateDataGatheringFrequency(
        DataGatherFreq newDataGatheringFrequency) {
    dataGatheringFrequency = newDataGatheringFrequency;
}

void TelemetryContext::updateWaterFlow(WaterFlow newWaterFlow) {
    Serial.println(newWaterFlow);
    waterFlow = newWaterFlow;
}

void TelemetryContext::setSleeping(bool isSleepTime) {
    sleepTime = isSleepTime;
}

bool TelemetryContext::shouldSleep() const {
    return sleepTime;
}

void TelemetryContext::setAutoWatering(bool watering) {
    shouldAutoWater = watering;
}

bool TelemetryContext::shouldWaterAutomatically() const {
    return shouldAutoWater;
}

void TelemetryContext::setMonitoring(bool isMonitorTime) {
    monitorTime = isMonitorTime;
}

bool TelemetryContext::shouldMonitor() const {
    return monitorTime;
}

bool TelemetryContext::isSoilHumidityLow() const {
    return soilDryness > soilHumidityMaxThreshold;
}

uint32_t TelemetryContext::getSoilHumidity() const {
    return soilDryness;
}

bool TelemetryContext::isSoilHumidityHigh() const {
    return soilDryness < soilHumidityMinThreshold;
}

void TelemetryContext::setManualWatering(bool watering) {
    shouldManuallyWater = watering;
}

bool TelemetryContext::shouldWaterManually() const {
    return shouldManuallyWater;
}

uint32_t TelemetryContext::getLastDataGatherTime() const {
    return lastDataGatherTime;
}

void TelemetryContext::updateLastDataGatherTime() {
    uint32_t newTime = millis();
    uint32_t dt = newTime - lastDataGatherTime;
    if (dt > 0) {
        lastDataGatherTime = newTime;
    }
}

uint32_t TelemetryContext::getLastTankLevelUpdateTime() const {
    return lastTankLevelUpdateTime;
}

void TelemetryContext::updateLastTankLevelUpdateTime() {
    uint32_t newTime = millis();
    uint32_t dt = newTime - lastTankLevelUpdateTime;
    if (dt > 0) {
        lastTankLevelUpdateTime = newTime;
    }
}
