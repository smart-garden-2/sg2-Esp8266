#ifndef SMART_GDN_ESP8266_FLOWBOUND_H
#define SMART_GDN_ESP8266_FLOWBOUND_H

#include "WaterFlow.h"

class FlowBound {
public:
    FlowBound(){
        open_angle = 0;
    }

    void updateFlow(WaterFlow flow) {
        switch(flow) {
            case ONE_QUARTER:
                open_angle = 45;
                break;
            case HALF:
                open_angle = 90;
                break;
            case THREE_QUARTER:
                open_angle = 135;
                break;
            case FULL:
                open_angle = 180;
                break;
        }
    }

protected:
    uint8_t open_angle; // from 0 to 180
};

#endif //SMART_GDN_ESP8266_FLOWBOUND_H
