#ifndef SMART_GARDEN_ESP8266_SOILMOISTURE_H
#define SMART_GARDEN_ESP8266_SOILMOISTURE_H

#include <Arduino.h>
// 735 upper, 375 lower, 555 mid

class SoilMoisture {

public:
    explicit SoilMoisture(int analogPin);
    uint32_t getSoilHumidity() const;

private:
//    const int drySoil = 260;
//    const int soakedSoil = 520;
    int analogPin;
};

#endif //SMART_GARDEN_ESP8266_SOILMOISTURE_H
