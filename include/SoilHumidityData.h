#ifndef SMART_GARDEN_ARDUINO_SOIL_HUMIDITY_DATA_H
#define SMART_GARDEN_ARDUINO_SOIL_HUMIDITY_DATA_H

struct SoilHumidityData {
    char topic[32];
    int min;
    int max;
};

#endif //SMART_GARDEN_ARDUINO_SOIL_HUMIDITY_DATA_H
