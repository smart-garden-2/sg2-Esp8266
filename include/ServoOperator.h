#ifndef SMART_GARDEN_ESP8266_SERVOOPERATOR_H
#define SMART_GARDEN_ESP8266_SERVOOPERATOR_H

#include <Arduino.h>
#include <Servo.h>
#include "FlowBound.h"
#include "TelemetryContext.h"

class ServoOperator: public FlowBound {

public:
    explicit ServoOperator(uint8_t servoPin);
    void on();
    void off();

private:
    Servo servo;
    uint8_t servoPin;
    bool tankOpen;
};

#endif //SMART_GARDEN_ESP8266_SERVOOPERATOR_H
