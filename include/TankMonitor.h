#ifndef SMART_GARDEN_ESP8266_TANKMONITOR_H
#define SMART_GARDEN_ESP8266_TANKMONITOR_H

#include "Sonar.h"
#include "IOMQTT.h"
#include <time.h>
#include "TelemetryContext.h"
#include "FrequencyBound.h"
#include "IOSerial.h"

class TankMonitor: public FrequencyBound {

public:
    TankMonitor(Sonar* sonar);
    void execute();

private:
    Sonar* sonar;
    time_t now{};
};

#endif //SMART_GARDEN_ESP8266_TANKMONITOR_H
