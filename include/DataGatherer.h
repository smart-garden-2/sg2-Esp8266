#ifndef SMART_GARDEN_ESP8266_DATAGATHERER_H
#define SMART_GARDEN_ESP8266_DATAGATHERER_H

#include <Arduino.h>
#include <IOSerial.h>
#include "SoilMoisture.h"
#include "IOMQTT.h"
#include <time.h>
#include <Ticker.h>
#include "TelemetryContext.h"
#include "FrequencyBound.h"
#include "Secrets.h"

class DataGatherer: public FrequencyBound {

public:
    DataGatherer(SoilMoisture* soilMoisture, uint32_t soilDrynessSimulated);
    void execute();

private:
//    BME* bme;
    SoilMoisture* soilMoisture;
    time_t now{};
    uint32_t soilDrynessSimulated;
    uint32_t lastSoilDrynessSwap;
    void publishData(
            float airHumidity,
            uint32_t soilHumidity,
            float temperature,
            uint32_t pressure);
};

#endif //SMART_GARDEN_ESP8266_DATAGATHERER_H
