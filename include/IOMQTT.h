#ifndef SMART_GARDEN_ESP8266_IOMQTT_H
#define SMART_GARDEN_ESP8266_IOMQTT_H

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "ArduinoJsonWrapper.h"
#include <PubSubClient.h>
#include "TelemetryContext.h"
#include "Secrets.h"
#include <time.h>
#include "Msg.h"
#include "StateData.h"
#include "SoilHumidityData.h"

#ifdef DEBUG
#include <IOSerial.h>
#endif

class IOMQTTClass {

public:
    static void subscribe(const char* topic);
    static void publish(const JsonDocument& jsonDocument, const char* topic);
    static void publish(const char* msg, const char* topic);
    static void publishMetricAsJson(
            time_t now,
            const char* metricName,
            uint8_t metricValue,
            const char* topic);
    static bool connected();
    static void poll();

    void init();
    bool isMsgAvailable() const;
    Msg* receiveMsg();
    Msg* receiveMsgTopic();

    void connect();

    Msg* currentTopic;
    Msg* currentPayload;
    bool msgAvailable;
    bool rcvJson(StateData& data);
    bool rcvJson(SoilHumidityData& data);

private:
    bool rcvMsg(StateData& data, String& input);
    bool rcvMsg(SoilHumidityData& data, String& input);
    const char* broker = "6.tcp.eu.ngrok.io";
    const int port = 11770;
};

extern IOMQTTClass IOMQTT;

#endif //SMART_GARDEN_ESP8266_IOMQTT_H
