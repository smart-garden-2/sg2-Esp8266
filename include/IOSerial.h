#ifndef SMART_GARDEN_ESP8266_IOSERIAL_H
#define SMART_GARDEN_ESP8266_IOSERIAL_H

#include <Arduino.h>
#include "ArduinoJsonWrapper.h"
#include "Msg.h"

class IOSerialClass {

public:
    Msg* currentMsg;
    bool msgAvailable;
    void init();
    bool isMsgAvailable() const;
    Msg* receiveMsg();
    static void sendMsg(const String& msg);
};

extern IOSerialClass IOSerial;

#endif //SMART_GARDEN_ESP8266_IOSERIAL_H
