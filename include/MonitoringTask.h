#ifndef SMART_GARDEN_ESP8266_MONITORINGTASK_H
#define SMART_GARDEN_ESP8266_MONITORINGTASK_H

#include <Ticker.h>
#include "Task.h"
#include "DataGatherer.h"
#include "TankMonitor.h"

class MonitoringTask: public Task {

public:
    MonitoringTask(
            DataGatherer* dataGatherer,
            TankMonitor* tankMonitor,
            uint32_t period_ms);
    void setActive(bool active) override;
    void tick() override;

private:
    enum { IDLE, WORKING } state;
    Ticker timer;
    DataGatherer* dataGatherer;
    TankMonitor* tankMonitor;
};

#endif //SMART_GARDEN_ESP8266_MONITORINGTASK_H
