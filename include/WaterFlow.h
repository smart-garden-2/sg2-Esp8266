#ifndef SMART_GDN_ESP8266_WATERFLOW_H
#define SMART_GDN_ESP8266_WATERFLOW_H

typedef enum {
    ONE_QUARTER, HALF, THREE_QUARTER, FULL
} WaterFlow;

WaterFlow getFlow(const char* metric);

#endif //SMART_GDN_ESP8266_WATERFLOW_H
