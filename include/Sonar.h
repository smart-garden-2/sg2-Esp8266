#ifndef SMART_GARDEN_ESP8266_SONAR_H
#define SMART_GARDEN_ESP8266_SONAR_H

#include <Arduino.h>

class Sonar {

public:
    Sonar(int echoPin, int trigPin, uint32_t timeOut);
    float getDistance();
    void setTemperature(float temp);

private:
    const float vs = 331.5 + 0.6 * 20;
    float getSoundSpeed() const;
    float temperature;
    int echoPin, trigPin;
    uint32_t timeOut;
};

#endif //SMART_GARDEN_ESP8266_SONAR_H
