#ifndef SMART_GARDEN_ESP8266_FREQUENCYBOUND_H
#define SMART_GARDEN_ESP8266_FREQUENCYBOUND_H

#include "DataGatherFreq.h"

class FrequencyBound {
public:
    FrequencyBound(){
        dt = 0;
    }

    void updateFreq(DataGatherFreq freq) {
        switch (freq) {
            case EACH_SECOND: dt = 1000; break;
            case EACH_5SECOND: dt = 5000; break;
            case EACH_MINUTE: dt = 60000; break;
            case EACH_QUARTER: dt = 900000; break;
            case EACH_HOUR: dt = 3600000; break;
            case EACH_DAY: dt = 86400000; break;
        }
    }

protected:
    uint32_t dt; // millisec
};

#endif //SMART_GARDEN_ESP8266_FREQUENCYBOUND_H
