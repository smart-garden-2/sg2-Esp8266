#ifndef SMART_GARDEN_ESP8266_TELEMETRYCONTEXT_H
#define SMART_GARDEN_ESP8266_TELEMETRYCONTEXT_H

#include <Arduino.h>
#include "FrequencyBound.h"
#include "FlowBound.h"

class TelemetryContext {

public:
    TelemetryContext();
    void updateSoilHumidity(uint32_t newSoilHumidity);
    void updateSoilHumidityThreshold(
            uint32_t newSoilHumidityMinThreshold,
            uint32_t newSoilHumidityMaxThreshold);
    DataGatherFreq getDataGatheringFrequency() const;
    WaterFlow getWaterFlow() const;
    void updateWaterFlow(WaterFlow newWaterFlow);
    void updateDataGatheringFrequency(DataGatherFreq newDataGatheringFrequency);
    void setSleeping(bool isSleepTime);
    bool shouldSleep() const;
    void setAutoWatering(bool watering);
    bool shouldWaterAutomatically() const;
    void setMonitoring(bool isMonitorTime);
    bool shouldMonitor() const;
    bool isSoilHumidityLow() const;
    bool isSoilHumidityHigh() const;
    void setManualWatering(bool watering);
    bool shouldWaterManually() const;
    uint32_t getLastTankLevelUpdateTime() const;
    void updateLastTankLevelUpdateTime();
    uint32_t getLastDataGatherTime() const;
    void updateLastDataGatherTime();
    uint32_t getSoilHumidity() const;

private:
    uint32_t soilDryness;
    uint32_t soilHumidityMinThreshold;
    uint32_t soilHumidityMaxThreshold;
    uint32_t lastDataGatherTime;
    uint32_t lastTankLevelUpdateTime;
    DataGatherFreq dataGatheringFrequency;
    WaterFlow waterFlow;
    bool sleepTime;
    bool shouldAutoWater;
    bool monitorTime;
    bool shouldManuallyWater;
};

extern TelemetryContext context;

#endif //SMART_GARDEN_ESP8266_TELEMETRYCONTEXT_H
