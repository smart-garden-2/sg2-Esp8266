#ifndef SMART_GARDEN_ESP8266_TASKMANAGER_H
#define SMART_GARDEN_ESP8266_TASKMANAGER_H

#include <Ticker.h>
#include "IOMQTT.h"
#include "Task.h"
#include "TelemetryContext.h"
#include "StateData.h"
#include "IOSerial.h"
#include "SoilHumidityData.h"

class ManagerTask: public Task {

public:
    explicit ManagerTask(uint32_t period_ms);
    void tick() override;

private:
    enum class EnumState {
        IDLE,
        WATERING_AUTOMATIC,
        WATERING_MANUAL,
        MONITOR,
        ALARM
    };

    Ticker timer;
    EnumState state;
    time_t now{};
    uint8_t maxConnTries;
    uint8_t connTries;
    uint8_t maxAlarmTries;
    uint8_t alarmTries;
    uint32_t lastAlarmAttempt;
    uint32_t lastWateringStopAttempt;
    const uint32_t standardSleepTime = 3600;
    const uint32_t shortSleepTime = 600000000;
    uint32_t timeUntilRetry;
    uint32_t timeWatering;
    const uint8_t maxReconnectionAttempts = 3;
    const uint8_t maxAlarmAttempts = 3;
    static uint8_t getSystemState(EnumState state);
    void checkInbox();
    void publishStateUpdate(EnumState stateValue);
    void publishWateringStateUpdate(EnumState stateValue);
};

#endif //SMART_GARDEN_ESP8266_TASKMANAGER_H
