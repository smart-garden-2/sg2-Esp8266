#ifndef SMART_GARDEN_ESP8266_WATERINGTASK_H
#define SMART_GARDEN_ESP8266_WATERINGTASK_H

#include <Arduino.h>
#include "Task.h"
#include "TelemetryContext.h"
#include "ServoOperator.h"
#include "TankMonitor.h"
#include "DataGatherer.h"

class WateringTask: public Task {

public:
    WateringTask(
            ServoOperator* servoOperator,
            uint32_t period_ms);
    void setActive(bool active) override;
    void tick() override;

private:
    enum { IDLE, WORKING } state;
    Ticker timer;
    ServoOperator* servoOperator;
};

#endif //SMART_GARDEN_ESP8266_WATERINGTASK_H
